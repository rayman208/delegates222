﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        //delegate void PrintMethod();

        static void GoodMorning()
        {
            Console.WriteLine("GoodMorning");
        }
        static void GoodAfternoon()
        {
            Console.WriteLine("GoodAfternoon");
        }
        static void GoodEvening()
        {
            Console.WriteLine("GoodEvening");
        }

        static void Main(string[] args)
        {
            int hour;
            Console.Write("input current hour: ");
            hour = int.Parse(Console.ReadLine());

            Action method = null;

            if (hour >= 0 && hour <= 11)
            {
                method = () =>
                {
                    Console.WriteLine("GoodMorning");
                };
            }
            else if (hour >= 12 && hour <= 17)
            {
                method = GoodAfternoon;
            }
            else if (hour >= 18 && hour <= 23)
            {
                method = GoodEvening;
            }

            if (method != null)
            {
                method();
            }

            //method?.Invoke();

            Console.ReadKey();
        }
    }
}
