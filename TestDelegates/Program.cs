﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDelegates
{
    class Program
    {
        /*interface IComparable
        {
            bool Compare(int a, int b);
        }

        class SortSmallToBig : IComparable
        {
            public bool Compare(int a, int b)
            {
                return b < a;
            }
        }

        class SortBigToSmall : IComparable
        {
            public bool Compare(int a, int b)
            {
                return b > a;
            }
        }*/

        delegate bool CompareAlgorithm(int a, int b);

        static Random rnd = new Random();

        static void FillMas(int[] mas)
        {
            for (int i = 0; i < mas.Length; i++)
            {
                mas[i] = rnd.Next(0, 100);
            }
        }

        static void PrintMas(int[] mas)
        {
            for (int i = 0; i < mas.Length; i++)
            {
                Console.Write(mas[i] + " ");
            }
            Console.WriteLine();
        }

        static bool SortSmallToBig(int a, int b)
        {
            return b < a;
        }

        static bool SortBigToSmall(int a, int b)
        {
            return b > a;
        }

        static bool SortOddToEven(int a, int b)
        {
            return b % 2 == 0 && a % 2 != 0;
        }

        static void BubleSort(int[] mas, CompareAlgorithm compare)
        {
            bool sort;
            int temp;
            int spin = 0;

            do
            {
                sort = true;

                for (int i = 0; i < mas.Length - 1 - spin; i++)
                {
                    if (compare(mas[i], mas[i + 1]) == true)
                    {
                        temp = mas[i];
                        mas[i] = mas[i + 1];
                        mas[i + 1] = temp;

                        sort = false;
                    }
                }

                spin++;

            } while (sort == false);
        }


        static void Main(string[] args)
        {
            int[] mas = new int[10];
            FillMas(mas);
            PrintMas(mas);
            BubleSort(mas, (a, b) => b < a);
            PrintMas(mas);
            Console.ReadKey();


            Console.ReadKey();
        }
    }
}
